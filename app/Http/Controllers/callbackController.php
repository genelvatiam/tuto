<?php


namespace App\Http\Controllers;


use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Lcobucci\JWT\Parser;

class callbackController extends Controller
{

    public function callback1(Request $req){

        return redirect('/');
    }

    public function callback(Request $request)
    {

        $client = new \GuzzleHttp\Client;

        $publicKey = <<<EOD
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA190IHLBunQMsXc6XaXvX
15ao0emtbgjiGI2d9NiuCpoGUXapd2aPE0gzHwoynaZ9n996wcoLW+Y7HCjAaXCr
Ch6tlOP3iu2TRusLuybXza9mxejTeYQDBPwjvovCn8qejk5kOJUINRwVjhauo9yu
UEMyRSWRJ62W1MGAC2rzTe8knXBfwlrb7zScfyiQ9SijkqQWGTXDnoxntkJWXhm/
dnzvYkWkdNXyzlxY6BkQUP5A0YmfSFors0jbrNjQZyS0si11tyO7DsDjnQinQ0Ly
qU8ZxH0fi6UffOAmDPQ57H+hCcr3vdRz4pTk31fIy67HSjjHOjRlS2ewiubHykS0
5QIDAQAB
-----END PUBLIC KEY-----
EOD;

        if(isset($request['error']) && $request['error'] == 'access_denied') {
            return redirect('http://localhost:8000/ ');
        } else {

      $response = $client->post('http://localhost:8000/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => '1',
                'client_secret' => '5jkXTiosFuQniYe9hqhO5x41pZhzAkDXfWES8KAZ',
                'redirect_uri' => 'http://localhost:8001/callback',
                'code' => $request->get('code'),
            ],
        ]);

        $result = json_decode((string)$response->getBody(), true);

        if ($result['id_token'] != null) {
            $id_token = $result['id_token'];
            $token = $result['access_token'];
            $type_token = $result['token_type'];
            $ttl_token = $result['expires_in'];

            $token = (new Parser())->parse((string)$id_token);
            //$headers = $token->getHeaders(); // Retrieves the token header
            $claims = $token->getClaims(); // Retrieves the token claims
            //$json_decoded = json_decode($token, true);

            // dd($claims['email']->getValue(), $claims['email_verified'], $claims['id'], $claims['name'], $claims['admin'],$claims['created_at']);

            Session::put('email', $claims['email']->getValue());
            Session::put('id', $claims['id']->getValue());
            Session::put('name', $claims['name']->getValue());
            Session::put('admin', $claims['admin']->getValue());
            Session::put('created_at', $claims['created_at']->getValue());

            return redirect()->route('home');

           /* $user = array("id"=>$claims['id']->getValue(),
                "email"=>$claims['email']->getValue(),
                "name"=>$claims['name']->getValue(),
                "admin"=>$claims['admin']->getValue(),
                "date"=>$claims['created_at']->getValue()
            );*/
            //return redirect()->route('home')->with(['user'=>$user]);
           // return view('home', compact('user'));
        }else{
            return view('welcome');
        }
        }

    }
}
