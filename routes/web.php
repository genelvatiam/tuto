<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return redirect('/todo');
});

Route::get('/callback', 'callbackController@callback');

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/home', function () {

    return redirect('/todo');
})->name('home');

Route::resource('/todo', 'TodoController');




Route::auth();



